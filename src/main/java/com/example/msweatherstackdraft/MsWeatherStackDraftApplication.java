package com.example.msweatherstackdraft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.*;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.*;

@EnableFeignClients
@SpringBootApplication
@EnableScheduling
public class MsWeatherStackDraftApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsWeatherStackDraftApplication.class, args);
    }

}
