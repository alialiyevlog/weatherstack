package com.example.msweatherstackdraft.controller;

import com.example.msweatherstackdraft.dto.*;
import com.example.msweatherstackdraft.helper.validators.*;
import com.example.msweatherstackdraft.service.*;
import org.springframework.http.*;
import org.springframework.validation.annotation.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/current-weather")
@Validated
public class WeatherController {

    private final WeatherService weatherService;


    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/{city}")
    public ResponseEntity<WeatherDto> getWeatherData(@PathVariable("city") @ValidCityName String cityName) {

        return ResponseEntity.ok(weatherService.getCurrentWeather(cityName));
    }





}
