package com.example.msweatherstackdraft.controller;

import com.example.msweatherstackdraft.entity.user.*;
import com.example.msweatherstackdraft.service.*;

import lombok.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.security.core.context.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    @Autowired
    private final AccountService accountService;
    private final FileStorageService fileStorageService;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadProfileImage(@RequestParam("avatar")MultipartFile file) {
        //Get the username of logged-in user
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        String filePath = fileStorageService.uploadUserProfileImage(file, username);
        accountService.uploadAvatar(filePath, username);

        return ResponseEntity.ok().build();
    }
}
