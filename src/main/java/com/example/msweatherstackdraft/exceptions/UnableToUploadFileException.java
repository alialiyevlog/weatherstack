package com.example.msweatherstackdraft.exceptions;

public class UnableToUploadFileException extends RuntimeException{
    public UnableToUploadFileException(String message) {
        super(message);
    }
}
