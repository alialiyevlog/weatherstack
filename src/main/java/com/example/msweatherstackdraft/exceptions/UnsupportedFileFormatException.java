package com.example.msweatherstackdraft.exceptions;

public class UnsupportedFileFormatException extends RuntimeException{
    public UnsupportedFileFormatException(String message) {
        super(message);
    }
}
