package com.example.msweatherstackdraft.exceptions;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.time.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorObject {

    private Integer statusCode;
    private String message;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private LocalDateTime timestamp;

}
