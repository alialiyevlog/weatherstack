package com.example.msweatherstackdraft.helper.formatters;

import org.springframework.format.Formatter;
import org.springframework.stereotype.*;

import java.time.*;
import java.time.format.*;
import java.util.*;

@Component
public class DateAndTimeFormatter implements Formatter<LocalDateTime> {
    @Override
    public LocalDateTime parse(String text, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(text, formatter);
    }



        @Override
        public String print(LocalDateTime object, Locale locale) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return  object.format(formatter);
        }



}

