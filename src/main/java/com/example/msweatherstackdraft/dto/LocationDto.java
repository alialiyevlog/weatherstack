package com.example.msweatherstackdraft.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import lombok.experimental.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)

public class LocationDto {
    String name;
    String country;
    String region;
    String lat;
    String lon;
    @JsonProperty("timezone_id")
    String timezoneId;
    String localtime;
    @JsonProperty("localtime_epoch")
    int localtimeEpoch;
    @JsonProperty("utc_offset")
    String utcOffset;
}
