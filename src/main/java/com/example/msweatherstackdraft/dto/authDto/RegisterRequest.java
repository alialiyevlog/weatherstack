package com.example.msweatherstackdraft.dto.authDto;

import com.example.msweatherstackdraft.helper.validators.*;
import jakarta.validation.constraints.*;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {


    @NotBlank(message = "Username is required")
    private String userName;
    @Email(message = "Email should be valid")
    @NotEmpty(message = "Email is required")
    private String email;
    @ValidPassword
    private String password;

}
