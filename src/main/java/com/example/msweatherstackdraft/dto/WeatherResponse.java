package com.example.msweatherstackdraft.dto;

import com.example.msweatherstackdraft.exceptions.*;
import lombok.*;
import lombok.experimental.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WeatherResponse {

    private RequestDto request;
    private LocationDto location;
    private CurrentDto current;
    private boolean success;
    private ErrorObject error;

}


