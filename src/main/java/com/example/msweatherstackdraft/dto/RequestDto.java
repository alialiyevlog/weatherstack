package com.example.msweatherstackdraft.dto;

import lombok.*;
import lombok.experimental.*;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestDto {
    String type;
    String query;
    String language;
    String unit;
}
