package com.example.msweatherstackdraft.client;


import com.example.msweatherstackdraft.dto.*;
import org.springframework.cloud.openfeign.*;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "weatherStack", url = "${weather.api.base-url}")
public interface WeatherStackApiCall {

    @GetMapping("/current")
    WeatherResponse getCurrentWeatherResponse(@RequestParam("access_key") String accessKey, @RequestParam("query") String query);
}
