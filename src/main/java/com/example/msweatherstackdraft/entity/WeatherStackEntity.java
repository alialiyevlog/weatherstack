package com.example.msweatherstackdraft.entity;

import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.*;
import org.hibernate.annotations.*;
import org.springframework.context.annotation.*;

import java.time.*;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "weather_stack")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WeatherStackEntity {
    @Id
    @GeneratedValue(generator = "hibernate-uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "uuid", unique = true)

    UUID id;
    String requestedCityName;
    String cityName;
    String country;
    String temperature;
    LocalDateTime updateTime;
    LocalDateTime responseLocalTime;

}
