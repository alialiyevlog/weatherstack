package com.example.msweatherstackdraft.repository;

import com.ulviglzd.weatherapi.entity.weather.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

import java.util.*;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherEntity, UUID> {

    Optional<WeatherEntity> findByRequestedCityName(String cityName);
}
