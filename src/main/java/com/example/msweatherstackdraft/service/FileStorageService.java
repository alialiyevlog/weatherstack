package com.example.msweatherstackdraft.service;

import org.springframework.web.multipart.*;

public interface FileStorageService {
    String uploadUserProfileImage(MultipartFile file, String username);
}
