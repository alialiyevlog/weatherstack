package com.example.msweatherstackdraft.service.impl;


import com.example.msweatherstackdraft.exceptions.*;
import com.example.msweatherstackdraft.helper.formatters.*;
import com.example.msweatherstackdraft.helper.validators.*;
import com.example.msweatherstackdraft.service.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.core.io.*;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.*;

import java.io.*;
import java.text.*;
import java.util.*;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Autowired
    private ResourceLoader resourceLoader;
    @Value("${image.storage.path}")
    private String folderPath;
    private static final Logger log = LoggerFactory.getLogger(FileStorageService.class);


    @Override
    public String uploadUserProfileImage(MultipartFile imageFile, String username) {
        String filePath = null;

        //Check if filetype is allowed
        String contentType = imageFile.getContentType();
        if(!AllowedFileTypes.isImageAllowed(contentType)){
            throw new UnsupportedFileFormatException("Invalid file format. Only PNG, JPEG, and JPG images are allowed.");
        }

        try {
            //getting the absolute path of the folder
            String absoluteFilePath = resourceLoader
                    .getResource(folderPath)
                    .getFile()
                    .getAbsolutePath();

            SimpleDateFormat dateFormat = CustomDateFormatter.dateFormatToConcatToFiles;
            String timestamp = dateFormat.format(new Date());

            //creating unique filename
            filePath = absoluteFilePath + "\\" + username + timestamp + "_" + imageFile.getOriginalFilename();
            imageFile.transferTo(new File(filePath));

            log.info("File uploaded to: " + filePath + "successfully.");

        } catch (IOException e) {
            log.error("Unable to upload file: " + e.getMessage());
            throw new UnableToUploadFileException(e.getMessage());
        }

        return filePath;
    }


}
