package com.example.msweatherstackdraft.service.impl;


import com.example.msweatherstackdraft.repository.*;
import com.example.msweatherstackdraft.service.*;
import lombok.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    @Autowired
    private final UserRepository userRepository;

    @Override
    public void uploadAvatar(String filePath, String userName) {
        userRepository.findByUserName(userName).ifPresent(user -> {
            user.setUserImg(filePath);
            userRepository.save(user);
        });
    }

}

