package com.example.msweatherstackdraft.service.impl;


import com.example.msweatherstackdraft.repository.*;
import com.example.msweatherstackdraft.service.*;
import org.springframework.stereotype.*;

@Service
public class DataCleaningServiceImpl implements DataCleaningService {
    private final WeatherRepository weatherRepository;

    public DataCleaningServiceImpl(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    public void eraseOldData() {
        weatherRepository.deleteAll();
    }
}
