package com.example.msweatherstackdraft.service.impl;

import com.example.msweatherstackdraft.client.*;
import com.example.msweatherstackdraft.dto.*;
import com.example.msweatherstackdraft.entity.*;
import com.example.msweatherstackdraft.exceptions.*;
import com.example.msweatherstackdraft.helper.formatters.*;
import com.example.msweatherstackdraft.repository.*;
import com.example.msweatherstackdraft.service.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;

import java.time.*;
import java.util.*;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
//    private ModelMapper modelMapper;
    private final WeatherStackApiCall apiCall;
    private final WeatherRepository weatherRepository;

    @Value("${weather.api.key}")
    private String weatherApiKey;

    @Autowired
    private DateAndTimeFormatter dateAndTimeFormatter;

    private static final Logger log = LoggerFactory.getLogger(WeatherService.class);
    private final MailSendingService mailSendingService;
    private final UserRepository userRepository;


    public WeatherServiceImpl(WeatherStackApiCall apiCall,
                              WeatherRepository weatherRepository, MailSendingService mailSendingService, UserRepository userRepository) {
        this.apiCall = apiCall;
        this.weatherRepository = weatherRepository;
        this.mailSendingService = mailSendingService;
        this.userRepository = userRepository;
    }


    @Override
    public WeatherDto getCurrentWeather(String cityName) {

        Optional<WeatherStackEntity> weatherEntity = weatherRepository.findByRequestedCityName(cityName);
        WeatherDto weatherDTO = null;

        if (weatherEntity.isPresent() && !isWeatherDataExpired(weatherEntity.get())) {
            log.info("Weather data found in database for city: " + cityName);
            weatherDTO = convertWeatherEntityToWeatherDTO(weatherEntity.get());
        }
        else if (weatherEntity.isPresent() && isWeatherDataExpired(weatherEntity.get())) {
            log.info("Weather data expired in database for city: " + cityName);
            weatherRepository.delete(weatherEntity.get());
            WeatherStackEntity updatedWeatherDataFromApi = weatherRepository.save(generateWeatherEntity(cityName));
            weatherDTO = convertWeatherEntityToWeatherDTO(updatedWeatherDataFromApi);
        }
        else {
            log.info("Weather data not found in database for city: " + cityName);
            WeatherStackEntity updatedWeatherDataFromApi = weatherRepository.save(generateWeatherEntity(cityName));
            weatherDTO = convertWeatherEntityToWeatherDTO(updatedWeatherDataFromApi);
        }

        mailSendingService.sendMail(getUserEmail(), "Weather info for " + cityName,
                convertWeatherDTOtoMailText(weatherDTO));

        return weatherDTO;

    }


    private WeatherStackEntity generateWeatherEntity(String cityName) {

        //Sending request to weather api via WeatherStackApiCall
        WeatherResponse weatherResponse = apiCall.getCurrentWeatherResponse(weatherApiKey, cityName);
        if (weatherResponse.getError() != null) {
            throw new NoSuchCityException("No such city: " + cityName + " found in database");
        }

        WeatherStackEntity weatherEntity = WeatherStackEntity.builder()
                .requestedCityName(cityName)
                .cityName(weatherResponse.getLocation().getName())
                .country(weatherResponse.getLocation().getCountry())
                .temperature(weatherResponse.getCurrent().getTemperature())
                .updateTime(LocalDateTime.now())
                .responseLocalTime(dateAndTimeFormatter
                        .parse(weatherResponse.getLocation().getLocaltime(), Locale.getDefault()))
                .build();
        return weatherEntity;
    }

    private boolean isWeatherDataExpired(WeatherStackEntity weatherEntity) {
        return weatherEntity.getUpdateTime().isBefore(LocalDateTime.now().minusMinutes(40));
    }

    private WeatherDto convertWeatherEntityToWeatherDTO(WeatherStackEntity weatherEntity) {
        return modelMapper.map(weatherEntity, WeatherDto.class);
    }

    private String getUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //checking if user is authenticated
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            UserDetails principal = (UserDetails) authentication.getPrincipal();
            String username = principal.getUsername();
            var user = userRepository.findByUserName(username)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            return user.getEmail();
        }
        return "Authentication failed";
    }

    private String convertWeatherDTOtoMailText(WeatherDto weatherDTO) {
        if (weatherDTO != null) {
            return "City: " + weatherDTO.getCityName() + "\n" +
                    "Country: " + weatherDTO.getCountry() + "\n" +
                    "Temperature: " + weatherDTO.getTemperature() + "\n" +
                    "Local time: " + dateAndTimeFormatter.print(weatherDTO.getUpdateTime(), Locale.getDefault());
        }
        return "No weather data found";
    }


}
