package com.example.msweatherstackdraft.service.impl;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.mail.*;
import org.springframework.mail.javamail.*;
import org.springframework.stereotype.*;

@Service
public class MailSendingService {

    private final JavaMailSender javaMailSender;

    public MailSendingService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    private static final Logger log = LoggerFactory.getLogger(MailSendingService.class);

    @Value("${spring.mail.username}")
    private String username;

    public void sendMail(String to, String subject, String text) {


        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);

        javaMailSender.send(message);

        log.info("Mail sent to: " + to);
    }
}
