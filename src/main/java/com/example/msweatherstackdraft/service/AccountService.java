package com.example.msweatherstackdraft.service;

public interface AccountService {
    void uploadAvatar(String filePath, String userName);
}
