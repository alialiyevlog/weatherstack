package com.example.msweatherstackdraft.service;

import com.example.msweatherstackdraft.dto.*;

public interface WeatherService {

    WeatherDto getCurrentWeather(String cityName);
}
