package com.example.msweatherstackdraft.service;

import org.springframework.stereotype.*;

@Service
public interface DataCleaningService {
    void eraseOldData();
}
